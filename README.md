# Projecte Curs Anàlisis Avançat

Aquest projecte és per constatar que he assistit al curs d' anàlisis quantitativa de la dada.

L'aplicació serveix per importar csv i crear gràfiques i poder-les pre-visualitzar-les com a gràfic de punts o com a gràfic de barres.
